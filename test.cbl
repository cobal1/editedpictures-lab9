       IDENTIFICATION DIVISION.
       PROGRAM-ID. CurrencyExample.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES.
           CURRENCY SIGN IS "£".

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  Amount       PIC 9(9)V99 VALUE 1234.56.

       PROCEDURE DIVISION.
           DISPLAY "Amount: " Amount.
           STOP RUN.
